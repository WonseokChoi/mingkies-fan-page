var gulp = require('gulp');
var sass = require('gulp-sass');
var autoprefixer = require('gulp-autoprefixer');
gulp.task('sass', function() {
    return gulp.src('src/scss/*.scss')
        .pipe(sass())
        .pipe(autoprefixer({ // autoprefixer 추가
            overrideBrowserslist: ['chrome > 0', 'ie > 0', 'firefox > 0']
        }))
        .pipe(gulp.dest('dist/css/'))
        .pipe(browserSync.stream({match:'**/*.css'})); // broswer-sync로 전송 추가
});
var spritesmith = require('gulp.spritesmith');
var imagemin = require('gulp-imagemin');
var buffer = require('vinyl-buffer');
gulp.task('sprite', function() {
    var spriteData = gulp.src('src/sprite/*.png')
        .pipe(spritesmith({
            imgName: 'sprite.png',
            cssName: 'sprite.scss',
            imgPath: './dist/img/sprite.png'
        }));
    var imgStream = new Promise(function(resolve) {
        spriteData.img
            .pipe(buffer())
            .pipe(imagemin())
            .pipe(gulp.dest('dist/img/'))
            .on('end',resolve);
    });
    var cssStream = new Promise(function(resolve) {
        spriteData.css
            .pipe(gulp.dest('src/scss/'))
            .on('end',resolve);
    });
    return Promise.all([imgStream, cssStream]);
});
